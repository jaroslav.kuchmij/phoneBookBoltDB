package cmd

import (
	"fmt"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/utils"
	"github.com/spf13/cobra"
	"log"
)

func init() {
	rootCmd.AddCommand(searchContactCmd)
}

// Команда поиск контакта по номеру телефона
var searchContactCmd = &cobra.Command{
	Use:   "searchContact",
	Short: "search Contact into data base",
	Long:  `This command searching Contact into data base by phone namber`,
	Run: func(cmd *cobra.Command, args []string) {
		err, db := utils.OpenDB() //открытие БД
		if err != nil{
			return
		}
		defer db.Close()  //После выполнения команды - закрытие БД
		fmt.Println("\t\tSearching")
		fmt.Print("Enter the phone number (format: +380981111111):")
		fmt.Scanln(&number)
		if !utils.CheckValidNumber(number) {	//проверка на формат номера
			fmt.Println("Invalid namber")
			return
		}
		if bool, err, item := utils.SearchNumberInDB(number, db); bool == true { //проверка на существование номера в файле
			utils.PrintStructContact(number, item) //Вывод на экран полученого контакта
			return
			if err != nil {
				log.Println("error search namber in data base: ", err)
				return
			}
		}
		log.Println("This number does not exist")
	},
}