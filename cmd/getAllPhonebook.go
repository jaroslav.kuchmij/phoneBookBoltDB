package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/utils"
	"log"
	"github.com/boltdb/bolt"
	"github.com/golang/protobuf/proto"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/model"
)

func init() {
	rootCmd.AddCommand(getContactsCmd)
}

// Команда чтения всех элементов из файла
var getContactsCmd = &cobra.Command{
	Use:   "getContacts",
	Short: "get all contacts of data base",
	Long:  `This command reads items into data base and print this items on the screen`,
	Run: func(cmd *cobra.Command, args []string) {
		contact := &model.Contact{}
		err, db := utils.OpenDB() //открытие БД
		if err != nil{
			return
		}
		defer db.Close()  //После выполнения команды - закрытие БД
		err = db.View(func(tx *bolt.Tx) error { //Открытие корзины для просмотра
			book := tx.Bucket([]byte(utils.NameBucket))
			book.ForEach(func(k, v []byte) error { //Итерация полученной корзины
				err = proto.Unmarshal(v, contact) //анмаршал значение корзины
				if err != nil{
					log.Println("error Unmarshal: ", err)
					return err
				}
				key := string(k) //преобразование ключа корзины к стринге
				utils.PrintStructContact(key, contact) //вывод контакта на экран
				return nil
			})
			return nil
		})
		if err != nil {
			log.Printf("error view in db: ", err)
			return
		}
	},
}