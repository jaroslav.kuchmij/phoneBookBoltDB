package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/utils"
	"log"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/model"
)

func init() {
	rootCmd.AddCommand(updateNameCmd)
}

// Команда изменения имение контакта по номеру телефона 
var updateNameCmd = &cobra.Command{
	Use:   "updateName",
	Short: "update Name into data base",
	Long:  `This command updating name contact. 
	Searching occurs on phone namber`,
	Run: func(cmd *cobra.Command, args []string) {
		contact := model.Contact{}
		err, db := utils.OpenDB() //открытие БД
		if err != nil{
			return
		}
		defer db.Close() //После выполнения команды - закрытие БД
		fmt.Println("\t\tUpdating number contact")
		fmt.Print("Enter the phone number (format: +380981111111):")
		fmt.Scanln(&number)
		if !utils.CheckValidNumber(number) {		//проверка на формат номера
			log.Println("Invalid namber")
			return
		}
		if bool, err, item := utils.SearchNumberInDB(number, db); bool == true { //проверка на существование номера в файле
			fmt.Println("Enter the new name")
			fmt.Scanln(&contact.Name) //через терминал передаем значение в поле структуры Contact
			item = &model.Contact{ //Запись входящих данных в структуру
				Name: contact.Name,
				Address: item.Address,
			}
			err = utils.MarshalAndWriteInDB(item, db, number) //Маршал вх данных и запись из в БД
			if err != nil {
				log.Print("error marshal and write in data base: ", err)
				return
			}
			log.Println("Update name")
			return
		}
		if err != nil {
			log.Println("error search namber in data base: ", err)
			return
		}
		log.Println("This number does not exist")
	},
}