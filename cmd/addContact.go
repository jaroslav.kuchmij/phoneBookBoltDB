package cmd

import (
        "gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/model"
        "github.com/spf13/cobra"
        "log"
        "gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/utils"
        "fmt"
        "github.com/boltdb/bolt"
        "strings"
        "bufio"
        "os"
)

var number string

func init() {
        rootCmd.AddCommand(addContactCmd)
}

var addContactCmd = &cobra.Command{
        Use:   "addContact",
        Short: "add contact from data base",
        Long:  `This command add phone number, name and address into data base.

	Format phone: +380111111111
	Format Name: free form
	Address: Voenomorskaya 3 4
	Example: +380984001650|Yaroslav|Voenomorskaya str.`,
        Run: func(cmd *cobra.Command, args []string) {
                contact := new(model.Contact)
                err, db := utils.OpenDB() //открытие БД
                if err != nil{
                        return
                }
                defer db.Close() //После выполнения команды - закрытие БД
                utils.CreateBucketInDB(db)
                if err != nil {
                        log.Println("error create Bucket DB: ", err)
                        return
                }
                errInt, st := setContactInStruct(contact, db) //Запись в струткруру значений
                if errInt != 0 {
                        return
                }
                err = utils.MarshalAndWriteInDB(st, db, number) //Маршал структуры и запись в БД
                if err != nil {
                        log.Print("error marshal and write in data base: ", err)
                        return
                }
                log.Print("Add contact")
        },
}

//Запись в структуру Contact данных введенных через терминал
func setContactInStruct(contact *model.Contact, db *bolt.DB) (int, *model.Contact)  {
        fmt.Println("Enter the phone number (format: +380981111111):")
        fmt.Scanln(&number) //через терминал передаем значение в поле структуры Contact
        //проверка на формат номера
        if !utils.CheckValidNumber(number) {
                log.Printf("Invalid number")
                return 1, nil
        }
        //проверка на существование номера в файле.
        if bool, err, _ := utils.SearchNumberInDB(number, db); bool == true {
                if err != nil {
                        log.Println("error serch namber", err)
                        panic(err)
                }
                log.Println("This number exists: ", number)
                return 1, nil
        }
        fmt.Println("Enter Name:")
        fmt.Scanln(&contact.Name) //через терминал передаем значение в поле структуры Contact
        fmt.Println("Enter the address. Format: {'Street'+' '+'NumberHouse'+' '+'NumberFlat'}")
        scan := bufio.NewScanner(os.Stdin)
        scan.Scan()
        spl := strings.Split(scan.Text(), " ") //разбивка введенного сообщение по пробелам
        if len(spl) != 3{ //Проверка на валидность данных
                log.Println("Invalid address")
                return 1, nil
        }
        contact = &model.Contact{ //Запись входящих данных в структуру
                Name: contact.Name,
                Address: &model.Contact_Address{
                        Street: spl[0],
                        NumberHouse: spl[1],
                        NumberFlat: spl[2],
                },
        }
        return 0, contact
}