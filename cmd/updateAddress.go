package cmd

import (
	"fmt"
	"strings"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/utils"
	"log"
	"bufio"
	"os"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/model"
)

func init() {
	rootCmd.AddCommand(updateAddressCmd)
}

// Команда изменения адреса контакта по номеру телефона 
var updateAddressCmd = &cobra.Command{
	Use:   "updateAddress",
	Short: "update Address into data base",
	Long:  `This command updating Address contact. 
	Searching occurs on phone namber`,
	Run: func(cmd *cobra.Command, args []string) {
		err, db := utils.OpenDB() //открытие БД
		if err != nil{
			return
		}
		defer db.Close()  //После выполнения команды - закрытие БД
		fmt.Println("\t\tUpdating address contact")
		fmt.Print("Enter the phone number (format: +380981111111):")
		fmt.Scanln(&number)
		if !utils.CheckValidNumber(number) {		//проверка на формат номера
			log.Println("Invalid namber")
			return
		}
		if bool, err, item := utils.SearchNumberInDB(number, db); bool == true { //проверка на существование номера в файле
			fmt.Println("Enter the new address. Format: {'Street'+' '+'NumberHouse'+' '+'NumberFlat'}")
			scan := bufio.NewScanner(os.Stdin)
			scan.Scan()
			spl := strings.Split(scan.Text(), " ") //разбивка введенного сообщение по пробелам
			if len(spl) != 3{ //Проверка на валидность данных
				log.Println("Invalid address")
				return
			}
			item = &model.Contact{ //Запись входящих данных в структуру
				Name: item.Name,
				Address: &model.Contact_Address{
					Street: spl[0],
					NumberHouse: spl[1],
					NumberFlat: spl[2],
				},
			}
			err = utils.MarshalAndWriteInDB(item, db, number) //Маршал вх данных и запись из в БД
			if err != nil {
				log.Print("error marshal and write in data base: ", err)
				return
			}
			log.Println("Update address")
			return
		}
		if err != nil {
			log.Println("error search namber in data base: ", err)
			return
		}
		log.Println("This number does not exist")
	},
}