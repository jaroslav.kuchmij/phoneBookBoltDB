package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/utils"
	"log"
	"github.com/boltdb/bolt"
)


func init() {
	rootCmd.AddCommand(deleteContactCmd)
}

// Команда удаления контакта по номеру телефона 
var deleteContactCmd = &cobra.Command{
	Use:   "deleteContact",
	Short: "delete contact into data base",
	Long:  `This command deleting contact in data base.
	Searching occurs on phone namber`,
	Run: func(cmd *cobra.Command, args []string) {
		err, db := utils.OpenDB() //открытие БД
		if err != nil{
			return
		}
		defer db.Close() //После выполнения команды - закрытие БД
		fmt.Println("\t\tDelete contact")
		fmt.Print("Enter the phone number (format: +380981111111):")
		fmt.Scanln(&number)
		if !utils.CheckValidNumber(number) {		//проверка на формат номера
			log.Println("Invalid namber")
			return
		}
		//проверка на существование номера в файле.
		if bool, err, _ := utils.SearchNumberInDB(number, db); bool == false {
			if err != nil {
				log.Print("error serch namber", err)
				return
			}
			log.Println("This number does not exist")
			return
		}
		err = db.Update(func(tx *bolt.Tx) error { //зменение корзины
			bckt := tx.Bucket([]byte(utils.NameBucket)) //Получение корзины по наименованию
			err := bckt.Delete([]byte(number)) //Удаление записи в корзине по ключу
			return err
		})
		if err != nil {
			log.Println("error delete key: ", err)
			return
		}
		log.Println("Contact deleted")
	},
}