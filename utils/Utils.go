package utils

import (
	"regexp"
	"gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/model"
	"fmt"
	"github.com/golang/protobuf/proto"
	"log"
	"github.com/boltdb/bolt"
)

const NameDB = "My.db"
const NameBucket = "PhoneBook"

//Открытие БД
func OpenDB() (error, *bolt.DB) {
	db, err := bolt.Open(NameDB, 0600, nil) //открытие БД
	if err != nil{
		log.Println("error Open data base: ", err)
	}
	return err, db
}

func CreateBucketInDB(db *bolt.DB) error {
	err := db.Update(func(tx *bolt.Tx) error { //изменение БД
		_, err := tx.CreateBucketIfNotExists([]byte(NameBucket)) //создание Bucket
		if err != nil {
			log.Println("error CreateBucketIfNotExists() DB: ", err)
		}
		return err
	})
	if err != nil {
		log.Println("error create bucket in DB: ", err)
		return err
	}
	log.Println("DB create")
	return err
}

//Сириализация
func MarshalAndWriteInDB(contact *model.Contact, db *bolt.DB, ID string)  error {
	out, err := proto.Marshal(contact) //перевод Структуры в формат ProtoBuf (сирилизация)
	if err != nil{
		log.Println("marshal error: ", err)
	}
	err = db.Update(func(tx *bolt.Tx) error { //Изменение БД
		err = tx.Bucket([]byte(NameBucket)).Put([]byte(ID), out) //Получение необходимой корзины и добавление новой записи
		if err != nil {
			log.Println("error put in bucket: ", err)
		}
		return err
	})
	if err != nil {
		log.Println("error update db: ", err)
	}
	return err
}

//Поиск совпадений по номеру в файле
func SearchNumberInDB(incomingValue string, db *bolt.DB)  (bool, error, *model.Contact) {
	boolSwitch := false // если Истина - совпадение есть, иначе нет совпадений
	contact := model.Contact{}
	err := db.View(func(tx *bolt.Tx) error { //Открытие БД для чтения
		if val := tx.Bucket([]byte(NameBucket)).Get([]byte(incomingValue)); val != nil { //Получение значения по ключу
			err := proto.Unmarshal(val, &contact) //Анмаршал значения и запись в структуру данных
			if err != nil{
				log.Println("error Unmarshal: ", err)
				panic(err)
			}
			boolSwitch = true // если есть совпадение указываем Истину
			return nil
		}
		return nil
	})
	if err != nil {
		log.Println("error view: ", err)
	}
	return boolSwitch, err, &contact
}

//Проверка входящего номера на корректность
func CheckValidNumber(incomingValue string) bool {
	var validID = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`) //формат номера
	res := validID.MatchString(incomingValue)
	return res
}

//Вывод структуры телефонной книги на экран
func PrintStructContact(number string, contact *model.Contact)  {
	fmt.Println("Contact number: ", number)
	fmt.Println("Contact name: ", contact.Name)
	fmt.Println("Contact address: ", contact.Address.Street+" "+contact.Address.NumberHouse+" "+contact.Address.NumberFlat)
	fmt.Println("-------------------------------------------------------")
}