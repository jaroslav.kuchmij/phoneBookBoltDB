# Project "PhoneBook on BoltDB"

## Данный репозиторий содержит самостоятельный проект на языке GO 
 
### Условия задачи:
1. Проект должен быть создан на базе COBRA
2. Проект использует ProtoBuf
2. Проект работает с BoltDB

### Инструкция по создания проекта:
1. Установка необходимых инструментов:
    - установка Cobra. В терминале
    <pre>
    go get github.com/spf13/cobra/cobra
    </pre>
    Подробо о том, как создать проект на Cobra можно ознакомится по [*ссылке*] (https://gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct)
    - установка ProtoBuf. В терминале:
    <pre>
    go get github.com/golang/protobuf/protoc-gen-go
    </pre>
    Подробо о том, как создать проект c использованием ProtoBuf можно ознакомится по [*ссылке*] (https://gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf)
    - установка BoltDB. В терминале
    <pre>
    go get github.com/boltdb/bolt/...
    </pre>
2. Создать проект COBRA. Перейти в консоле в директорию %GOPATH%\bin и набрать команду 
<pre>
cobra init %GOPATH%/[Путь к проекту] (Например: E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookCobraSruct.git)
</pre>
3. Создать proto файл в пакете model(gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git/model)
4. В консоле перейти в $GOPATH/bin/protoc-3.5.0-win32/bin и запустить комманду:
<pre>
protoc -I=$GOPATH\src[имя проекта] --go_out=$GOPATH\src[имя проекта] $GOPATH\src[имя проекта]\model\Contact.proto
</pre>
5. Далее создать COBRA команды используя структуру protoBuf. Необходимо использовать BoltDB для хранения данных.
В командах необходимо использовать библиотеку BoltDB

### Запуск проекта:
1. Скачать проект - 
<pre>
go get gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git
</pre>
2. Перейти в терминале в папку  %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookBoltDB.git
3. Ввыполнить команду go run main.go <command>:
    - addContact - добавление контакта
    - searchContact - поиск контакта по номеру телефона
    - getContacts - получение всего списка контактов
    - updateName - изменение имени по телефону
    - updateAddress - изменение адреса по номеру телефона
    - deleteContact - удаление контакта по номеру телефона

### BoltDB

[*Документация*](https://github.com/boltdb/bolt)

**~~Создатель кода~~** Главный бездельник - [*YaroslavKuchmiy*](https://www.facebook.com/profile.php?id=100006520172879)